Catalog
====================

Catalog service.

### Running

* `docker-compose up -d`

* `docker-compose exec php bash`

    * `composer install`
    * `bin/console doctrine:migrations:migrate`

### Documentation

* Postman

    https://documenter.getpostman.com/view/8776767/SVn2PvjU

* OpenAPI

    https://app.swaggerhub.com/apis-docs/dvlpm/Catalog/1.0.0
