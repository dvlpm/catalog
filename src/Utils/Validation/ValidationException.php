<?php

namespace App\Utils\Validation;

use Symfony\Component\Validator\ConstraintViolationList;

class ValidationException extends \Exception
{
    private ConstraintViolationList $violationList;

    public function __construct(ConstraintViolationList $violationList)
    {
        $this->violationList = $violationList;
        parent::__construct((string)$violationList, 422, null);
    }

    public function getViolationList(): ConstraintViolationList
    {
        return $this->violationList;
    }
}
// @TODO move to package
