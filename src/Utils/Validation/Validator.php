<?php

declare(strict_types=1);

namespace App\Utils\Validation;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\GroupSequence;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\ConstraintViolationList;

class Validator
{
    private ValidatorInterface $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param mixed $value
     * @param Constraint|Constraint[]|null $constraints
     * @param string|GroupSequence|(string|GroupSequence)[]|null $groups
     *
     * @throws ValidationException
     */
    public function validate(object $value, $constraints = null, $groups = null): void
    {
        /** @var ConstraintViolationList $constraintViolationList */
        $constraintViolationList = $this->validator->validate($value, $constraints, $groups);

        if ($constraintViolationList->count()) {
            throw new ValidationException($constraintViolationList);
        }
    }
}
// @TODO move to package
