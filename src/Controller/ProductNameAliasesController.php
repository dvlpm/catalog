<?php declare(strict_types=1);

namespace App\Controller;

use App\Command\AppendProductNameAliasesCommand;
use App\CommandHandler\AppendProductNameAliasesCommandHandler;
use App\CommandHandler\RuntimeCommandHandleException;
use App\Repository\ProductReadRepository;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ProductNameAliasesController extends AbstractController
{
    /**
     * @Route("/products/{productUuid}/name-aliases", methods={"GET"})
     *
     * @param ProductReadRepository $readRepository
     * @param string $productUuid
     * @return JsonResponse
     * @throws EntityNotFoundException
     */
    public function list(ProductReadRepository $readRepository, string $productUuid): JsonResponse
    {
        $product = $readRepository->findByUuid($productUuid);

        if ($product === null) {
            throw new EntityNotFoundException('No product found');
        }

        return $this->json($product->getNameAliases());
    }

    /**
     * @Route("/products/{productUuid}/name-aliases", methods={"POST"})
     *
     * @param AppendProductNameAliasesCommandHandler $commandHandler
     * @param AppendProductNameAliasesCommand $command
     * @return JsonResponse
     * @throws RuntimeCommandHandleException
     */
    public function appendList(
        AppendProductNameAliasesCommandHandler $commandHandler,
        AppendProductNameAliasesCommand $command
    ): JsonResponse {
        $product = $commandHandler->handle($command);

        return $this->json($product->getNameAliases());
    }
}
