<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Product;

class ProductWriteRepository extends AbstractWriteRepository
{
    public function save(Product $product): void
    {
        $this->doSave($product);
    }

    public function update(Product $product): void
    {
        $this->doUpdate($product);
    }
}
