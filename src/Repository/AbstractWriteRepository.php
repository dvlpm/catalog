<?php

declare(strict_types=1);

namespace App\Repository;

use Doctrine\ORM\EntityManager;

abstract class AbstractWriteRepository
{
    private EntityManager $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function beginTransaction(): void
    {
        $this->entityManager->beginTransaction();
    }

    public function commit(): void
    {
        $this->entityManager->commit();
    }

    public function rollback(): void
    {
        $this->entityManager->rollback();
    }

    protected function doSave(object $entity): void
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush($entity);
    }

    protected function doUpdate(object $entity): void
    {
        $this->entityManager->flush($entity);
    }

    protected function doDelete(object $entity): void
    {
        $this->entityManager->remove($entity);
    }
}
// @TODO move to package
