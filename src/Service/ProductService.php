<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Product;
use App\Repository\ProductWriteRepository;
use App\Utils\Validation\Validator;

class ProductService
{
    private ProductWriteRepository $productWriteRepository;
    private Validator $validator;

    public function __construct(ProductWriteRepository $productWriteRepository, Validator $validator)
    {
        $this->productWriteRepository = $productWriteRepository;
        $this->validator = $validator;
    }

    public function create(Product $product): void
    {
        $this->validator->validate($product);
        $this->productWriteRepository->save($product);
    }

    public function update(Product $product): void
    {
        $this->validator->validate($product);
        $this->productWriteRepository->update($product);
    }
}
