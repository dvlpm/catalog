<?php

declare(strict_types=1);

namespace App\CommandHandler;

use App\Command\AppendProductNameAliasesCommand;
use App\Entity\Product;
use App\Repository\ProductReadRepository;
use App\Service\ProductService;

class AppendProductNameAliasesCommandHandler
{
    private ProductReadRepository $productReadRepository;
    private ProductService $productService;

    public function __construct(ProductReadRepository $productReadRepository, ProductService $productService)
    {
        $this->productReadRepository = $productReadRepository;
        $this->productService = $productService;
    }

    /**
     * @param AppendProductNameAliasesCommand $command
     * @return Product
     * @throws RuntimeCommandHandleException
     */
    public function handle(AppendProductNameAliasesCommand $command): Product
    {
        $product = $this->productReadRepository->findByUuid($command->getProductUuid());

        if ($product === null) {
            throw new RuntimeCommandHandleException('Product not found');
        }

        foreach ($command->getNames() as $name) {
            if ($product->hasNameAlias($name)) {
                continue;
            }
            $product->addNameAlias($name);
        }

        try {
            $this->productService->update($product);
        } catch (\Exception $exception) {
            throw new RuntimeCommandHandleException($exception->getMessage(), $exception->getCode(), $exception);
        }

        return $product;
    }
}
