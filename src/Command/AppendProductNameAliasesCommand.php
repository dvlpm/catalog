<?php declare(strict_types=1);

namespace App\Command;
use Symfony\Component\Serializer\Annotation\Groups;

class AppendProductNameAliasesCommand
{
    /**
     * @Groups("append-product-name-aliases-command")
     *
     * @var string[]
     */
    private array $names;
    /**
     * @Groups("append-product-name-aliases-command")
     */
    private string $productUuid;

    public function __construct(array $names, string $productUuid)
    {
        $this->names = $names;
        $this->productUuid = $productUuid;
    }

    public function getNames(): array
    {
        return $this->names;
    }

    public function getProductUuid(): string
    {
        return $this->productUuid;
    }
}
