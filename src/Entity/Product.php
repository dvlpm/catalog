<?php

namespace App\Entity;

use App\Entity\Traits\NotNullableUuid;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductReadRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Product
{
    use NotNullableUuid;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="guid", unique=true)
     * @Groups("product")
     */
    private ?string $uuid = null;

    /**
     * @ORM\Column(type="array", nullable=true)
     * @Groups("product")
     * @Assert\NotBlank()
     */
    private array $nameAliases = [];

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("product")
     * @Assert\NotBlank()
     */
    private ?string $name = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getNameAliases(): ?array
    {
        return $this->nameAliases;
    }

    public function setNameAliases(?array $nameAliases): self
    {
        $this->nameAliases = $nameAliases;

        return $this;
    }

    public function hasNameAlias(string $nameAlias): bool
    {
        return in_array($nameAlias, $this->nameAliases, true);
    }

    public function addNameAlias(string $nameAlias): self
    {
        $this->nameAliases[] = $nameAlias;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
