<?php declare(strict_types=1);

namespace App\Entity\Traits;

use Ramsey\Uuid\Uuid;

trait NotNullableUuid
{
    abstract public function getUuid();
    abstract public function setUuid(string $uuid);

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function setUuidIfNull(): void
    {
        if ($this->getUuid() !== null) {
            return;
        }

        $this->setUuid(Uuid::uuid4()->toString());
    }
}
// @TODO move to package
