<?php

namespace App\Request\ParamConverter;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class QueryParamConverter implements ParamConverterInterface
{
    public function apply(Request $request, ParamConverter $configuration): bool
    {
        foreach ($request->query->all() as $key => $value) {
            $request->attributes->set($key, $value);
        }

        return true;
    }

    public function supports(ParamConverter $configuration): bool
    {
        return true;
    }
}
// @TODO move to package
