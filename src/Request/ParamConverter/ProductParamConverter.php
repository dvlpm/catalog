<?php

declare(strict_types=1);

namespace App\Request\ParamConverter;

use App\Entity\Product;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class ProductParamConverter extends AbstractObjectParamConverter
{
    protected array $groups = ['product'];

    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getClass() === Product::class;
    }
}
