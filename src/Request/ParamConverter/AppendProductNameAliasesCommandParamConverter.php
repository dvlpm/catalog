<?php

declare(strict_types=1);

namespace App\Request\ParamConverter;

use App\Command\AppendProductNameAliasesCommand;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class AppendProductNameAliasesCommandParamConverter extends AbstractObjectParamConverter
{
    protected array $groups = ['append-product-name-aliases-command'];

    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getClass() === AppendProductNameAliasesCommand::class;
    }

    protected function extractContentArrayFromRequest(Request $request): array
    {
        return [
            'productUuid' => $request->get('productUuid'),
            'names' => parent::extractContentArrayFromRequest($request)
        ];
    }

    protected function getObjectToPopulate(Request $request, ParamConverter $configuration): ?object
    {
        return null;
    }
}
